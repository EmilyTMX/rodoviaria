CREATE DATABASE rodoviaria;
USE rodoviaria;

CREATE TABLE passageiro (
  idPassageiro INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  genero VARCHAR(50) NOT NULL,
  rg VARCHAR(30) NOT NULL,
  cpf VARCHAR(30) NOT NULL,
  endereco  VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  telefone VARCHAR(55) NOT NULL,
  PRIMARY KEY (idPassageiro)
);

