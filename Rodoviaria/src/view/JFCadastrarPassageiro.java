package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.Font;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private JTextField txtGenero;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("Sistema Rodoviario - Cadastro de Passageiros");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCadastrarPassageiro = new JLabel("Cadastrar Passageiro");
		lblCadastrarPassageiro.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		lblCadastrarPassageiro.setBounds(118, 11, 159, 26);
		contentPane.add(lblCadastrarPassageiro);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(32, 51, 71, 14);
		contentPane.add(lblNome);
		
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setBounds(32, 79, 71, 14);
		contentPane.add(lblGenero);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setBounds(32, 104, 46, 14);
		contentPane.add(lblRG);
		
		JLabel lblCPF = new JLabel("CPF:");
		lblCPF.setBounds(184, 104, 46, 14);
		contentPane.add(lblCPF);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setBounds(32, 137, 71, 14);
		contentPane.add(lblEndereco);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(32, 199, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(32, 168, 71, 14);
		contentPane.add(lblTelefone);
		
		txtNome = new JTextField();
		txtNome.setBounds(73, 48, 273, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(86, 76, 263, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		txtRG = new JTextField();
		txtRG.setBounds(55, 104, 121, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(212, 104, 137, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(96, 134, 253, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
				
		txtTelefone = new JTextField();
		txtTelefone.setBounds(96, 196, 253, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(96, 165, 253, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		ButtonGroup genero = new ButtonGroup();
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				txtGenero.setText(null);
				txtRG.setText(null);
				txtCPF.setText(null);
				txtEndereco.setText(null);
				txtTelefone.setText(null);
				txtEmail.setText(null);
			}
		});
		btnLimpar.setBounds(32, 268, 89, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setNome(txtNome.getText());
				p.setGenero(txtGenero.getText());
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setTelefone(txtTelefone.getText());
				p.setRG(txtRG.getText());
				p.setCPF(txtCPF.getText());
			
				dao.create(p);
				dispose();
								
			}
		});
		
		btnCadastrar.setBounds(131, 268, 119, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(260, 268, 89, 23);
		contentPane.add(btnCancelar);
		
		
	}
}
