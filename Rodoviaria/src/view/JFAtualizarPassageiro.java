package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private JTextField txtGenero;

	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblIdFilme = new JLabel("ID Filme:");
		lblIdFilme.setBounds(220, 19, 59, 14);
		contentPane.add(lblIdFilme);
		
		JLabel lblID = new JLabel("000");
		lblID.setBounds(279, 19, 70, 14);
		contentPane.add(lblID);
		
		JLabel lblAlterarPassageiro = new JLabel("Alterar Passageiro");
		lblAlterarPassageiro.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		lblAlterarPassageiro.setBounds(32, 11, 159, 26);
		contentPane.add(lblAlterarPassageiro);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(32, 51, 71, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(78, 48, 86, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
			
		JLabel lblGenero = new JLabel("G\u00EAnero:");
		lblGenero.setBounds(32, 79, 71, 14);
		contentPane.add(lblGenero);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(78, 76, 86, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblRG = new JLabel("RG:");
		lblRG.setBounds(32, 104, 46, 14);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(68, 104, 86, 20);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF:");
		lblCPF.setBounds(204, 104, 46, 14);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(239, 101, 86, 20);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o:");
		lblEndereco.setBounds(32, 137, 71, 14);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(105, 134, 86, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone:");
		lblTelefone.setBounds(32, 168, 71, 14);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(105, 165, 86, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail:");
		lblEmail.setBounds(32, 199, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(88, 196, 86, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		

		lblID.setText(String.valueOf(p.getIdPassageiro()));
		txtNome.setText(p.getNome());
		txtGenero.setText(p.getGenero());
		txtRG.setText(p.getRG());
		txtCPF.setText(p.getCPF());
		txtEndereco.setText(p.getEndereco());
		txtTelefone.setText(p.getTelefone());
		txtEmail.setText(p.getEmail());
	
		
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				p.setIdPassageiro(Integer.parseInt(lblID.getText()));
				
				p.setNome(txtNome.getText());
				p.setGenero(txtGenero.getText());
				p.setEndereco(txtEndereco.getText());
				p.setRG(txtRG.getText());
				p.setCPF(txtCPF.getText());
				p.setTelefone(txtTelefone.getText());
				p.setEmail(txtEmail.getText());
			
				dao.update(p);
				dispose();
								
			}
		});
		
		btnAlterar.setBounds(131, 268, 119, 23);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				txtGenero.setText(null);
				txtRG.setText(null);
				txtCPF.setText(null);
				txtEndereco.setText(null);
				txtTelefone.setText(null);
				txtEmail.setText(null);
			}
		});
		btnLimpar.setBounds(32, 268, 89, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(260, 268, 89, 23);
		contentPane.add(btnCancelar);
		
	
	}
}
